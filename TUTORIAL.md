# Raise Your Hand Tutorial With the Daily Prebuilt

- [Introduction](#introduction)
  - [What is Raise Your Hand?](#what-is-raise-your-hand)
  - [What Will It Look Like?](#what-will-it-look-like)
- [The Setup](#the-setup)
- [Initialization](#initialization)
- [Toggling Your Hand State](#toggling-your-hand-state)
- [The Listeners](#the-listeners)
  - [joined-meeting](#joined-meeting)
  - [left-meeting](#left-meeting)
  - [participant-joined](#participant-joined)
  - [participant-left](#participant-left)
  - [participant-updated](#participant-updated)
  - [app-message](#app-message)
- [Finished!](#finished)

## Introduction

In this tutorial, we will use the Daily Prebuilt to add a `Raise Your Hand` feature to a Daily video call. This will
include:

- Allowing participants to raise their hand in a Daily Prebuilt video call
- Displaying which other participants have their hand raised
- Allowing participants to enable their camera when they raise their hand

The purpose of this tutorial is twofold:

- To show you how to build a useful Raise Your Hand feature with [Daily's JavaScript API](https://docs.daily.co/reference).
- To provide an example of how simple it can be to extend Daily's video calls with custom features in a way that is
  minimally intrusive to any existing call logic you may already have.

### What is Raise Your Hand?

`Raise Your Hand` is a feature which allows participants to click a link to "raise their hand" in a call.
The action notifies the call moderator, current speaker, and the other participants that you have something you'd
like to ask or say without interrupting whoever may currently be speaking.

A feature like `Raise Your Hand` allows
moderators to organize meetings and coordinate who is speaking in an orderly manner, making it very useful for calls
with a large number of participants.

### What Will It Look Like?

The end result will look like this (cats not included, sorry):

![Raise Your Hand example showing a Daily call with two cats on the left and the option to raise your hand on the right](example.png)

We will have the call frame on the left and a list of participants on the right. Underneath that, we'll have the button
allowing you to raise and lower your own hand and the option to enable turning on your video when your hand is raised.

Note that the participants list in the UI only shows participants who currently have their hand raised.
Of course, there are use cases for showing _all_ participants at all times regardless of hand state. But for our purposes
with this Daily Prebuilt implementation, that isn't necessary; the Daily Prebuilt already provides a list of
participants in the call within the iframe.

## The Setup

- Clone the [source](https://gitlab.com/drakonka/daily-raise-your-hand) for this tutorial so that you can follow along.
- Try out the [live demo](https://wonderful-khorana-9315ea.netlify.app/).
- run the demo locally, install dependencies via `npm i` and then run `npm run dev`.

In this tutorial, we will focus on building the `Raise Your Hand` feature on top of any existing call functionality you
may already have. For this purpose, we will use a stripped down version of the [Daily Prebuilt UI Demo](https://github.com/daily-demos/prebuilt-ui)
to handle our call creation/joining flows. I've simply removed unneeded features from that demo for brevity.

Let's get started!

## Initialization

In the head of our `index.html`, we'll need to import three scripts:

- The `daily-js` library.
- `raise-hand.js`, which actually holds our feature implementation.
- `index.js`, which contains our basic room-joining and setup logic.

```html
<script src="https://unpkg.com/@daily-co/daily-js"></script>
<script src="./raise-hand.js"></script>
<script src="./index.js"></script>
```

We will also define a small inline script here, which will call two functions: one to create our Daily call frame and
the second to initialize our `Raise your hand` listeners.

```html
<script>
  async function start() {
    await createCallframe();
    initRaiseYourHandListeners();
  }
</script>
```

This will be called on body `onload`:

```html
<body onload="start()"></body>
```

`createCallFrame()` will create our Daily call frame and set up some core listeners which we'll use to show errors and
toggle UI elements upon joining or leaving the call.

`initRaiseYourHandListeners()` will use the newly created global `callFrame` to set up the event listeners we'll be
using to update other participants' hand state in our local UI:

```javascript
function initRaiseYourHandListeners() {
  callFrame
    .on("joined-meeting", ryhHandleJoinedMeeting)
    .on("left-meeting", ryhHandleLeftMeeting)
    .on("participant-joined", ryhHandleParticipantJoined)
    .on("participant-left", ryhHandleParticipantLeft)
    .on("participant-updated", ryhHandleParticipantUpdated)
    .on("app-message", ryhHandleAppMessage);
}
```

Next, we'll prepare the DOM elements to display your raised hand and the raised hands of other participants:

```html
<div class="raised-hands">
  <h3>Raised Hands</h3>
  <ul>
    <div id="local"></div>
    <div id="other-participants"></div>
  </ul>
</div>
```

We also need to add the actual user controls to raise and lower the hand (as well as a checkbox to optionally have the
app turn on the user's camera when they have their hand raised):

```html
<div class="hand-controls">
  <h3>Raise Your Hand</h3>
  <p>
    Enable Camera when hand is raised:
    <input type="checkbox" id="do-toggle-hand-camera" />
  </p>
  <div class="custom-buttons">
    <button
      id="raise-hand"
      onclick="toggleRaiseHand()"
      class="button controls-button white-button"
    >
      Raise Hand &#x270B;
    </button>
  </div>
</div>
```

## Toggling Your Hand State

When a user clicks our button above, we'll toggle their hand state:

```javascript
function toggleRaiseHand() {
  userData.handRaised = !userData.handRaised;
  updateLocalParticipant();
  updateRaiseHandButton();
  maybeEnableCamera();
  sendLocalHandStateAppMessage(null);
}
```

The first thing we'll do is flip the `handRaised` boolean in the `userData` object to reflect the toggle.

Next, we will update the local participant UI:

```javascript
function updateLocalParticipant() {
  const localEle = document.getElementById("local");
  if (!userData.handRaised) {
    localEle.textContent = "";
    return;
  }
  let li = localEle.getElementsByTagName("li")[0];
  if (!li) {
    li = document.createElement("li");
  }
  li.textContent = `${userData.name} (You!) ${handEmoji}`;
  localEle.appendChild(li);
}
```

If the raised hand state was toggled to `false`, we'll simply remove the user from the UI. If the raised hand state was
toggled to `true`, we'll display the local user's name and the raised hand emoji.

Let's also update the Raise Hand button with a more relevant label:

```javascript
function updateRaiseHandButton() {
  const button = document.getElementById("raise-hand");
  let action = "";
  if (userData.handRaised) {
    action = "Lower";
  } else {
    action = "Raise";
  }
  button.textContent = `${action} Hand ${handEmoji}`;
}
```

Next, we will update the user's camera state:

```javascript
function maybeEnableCamera() {
  const checkBox = document.getElementById("do-toggle-hand-camera");
  const toggleCameraWithHand = checkBox.checked;
  if (!toggleCameraWithHand) {
    return;
  }
  const videoOn = callFrame.participants().local.video;
  if (userData.handRaised && !videoOn) {
    callFrame.setLocalVideo(true);
    return;
  }
}
```

In `maybeEnableCamera()` above, we first see whether the checkbox to enable the camera in response to the user's hand
state has been selected. If not, we simply early out.

If the user _did_ choose to have us enable the camera when they raise their hand, we check if their video camera is
already on. If their hand is raised, but their camera is _not_ on, we use `callFrame.setLocalVideo()` to enable their
camera for them.

For the sake of simplicity, we won't add logic to conditionally disable the video if the user lowers their hand here.
If this is something you'd like to implement for your version of `Raise Your Hand`, be sure to consider all the possible
flows very carefully. Manipulating a user's camera without thinking through the design can risk introducing annoying
behaviour and finicky error cases.

Finally, we broadcast our local hand state to all other participants via `sendLocalHandStateAppMessage()`:

```javascript
function sendLocalHandStateAppMessage(recipientSessionId) {
  let recipient = "*";
  let wantAck = false;
  if (recipientSessionId) {
    recipient = recipientSessionId;
    wantAck = true;
  }
  callFrame.sendAppMessage(
    { kind: "update", wantAck: wantAck, handRaised: userData.handRaised },
    recipient
  );
}
```

We pass `null` to this function because it will result in the recipient being set to `"*"`, which will send our message
to every participant in the room.

If we're sending our hand state to a specific participant, we'd also like them to acknowledge that they received it.
This is what the `wantAck` boolean is used for. This acknowledgement is needed to ensure newly joined participants
receive our up to date hand state message. We'll go into this in more detail below.

## The Listeners

We've covered our local hand state toggles. Now, let's take a look at the listeners which will let us see other
participants' hand states (as well as help us handle things like participant name changes).

We'll implement some very rudimentary error "handling" for the purpose of this tutorial. If one of the handlers
encounters an error, we'll simply log it to the console:

```javascript
function handleError(eventName, e) {
  console.error(`failed to handle ${eventName} event:`, e);
}
```

### joined-meeting

When the user joins a meeting, we call the `ryhHandleJoinedMeeting()` handler:

```javascript
function ryhHandleJoinedMeeting() {
  try {
    let participant = callFrame.participants().local;
    userData.sessionId = participant.session_id;
    userData.name = participant.user_name;
    prepCameraOptionCheckbox();
  } catch (err) {
    handleError("joined-meeting", err);
  }
}
```

The handler retrieves the local participant and saves their `session_id` and `user_name` in a small object.
It then preps the camera option checkbox, which will enable or disable the checkbox based on the user's video track
availability:

```javascript
function prepCameraOptionCheckbox() {
  // Disable video checkbox if the user's video track is unavailable
  const videoTrack = callFrame.participants().local.tracks.video;
  const cb = document.getElementById("do-toggle-hand-camera");
  if (videoTrack.state === "blocked") {
    cb.checked = false;
    cb.setAttribute("disabled", "true");
    return;
  }
  cb.removeAttribute("disabled");
}
```

The camera can be blocked if the user hasn't given camera permissions in their browser, or if their camera device is not
available for any reason.

### left-meeting

When the user leaves the meeting, we will reset their participants UI and local state:

```javascript
function ryhHandleLeftMeeting() {
  try {
    resetParticipants();
    resetLocal();
  } catch (err) {
    handleError("left-meeting", err);
  }
}
```

When resetting the participants, we'll just clear the inner content from the `other-participants` div:

```javascript
function resetParticipants() {
  const otherParticipantsEle = document.getElementById("other-participants");
  otherParticipantsEle.textContent = "";
}
```

When resetting the user's local state, we'll make sure their hand is lowered:

```javascript
function resetLocal() {
  userData.handRaised = false;
  updateLocalParticipant();
  updateRaiseHandButton();
}
```

### participant-joined

When a new participant joins the meeting, we want them to be able to see other people's current hand state.
In essence, it is as simple as sending the local hand state to the newly joined participant. However, there's a problem:
sometimes the `participant-joined` event is triggered for existing participants _before_ the newly joined participant
is ready to receive the status update we'll send them. This can result in our update message being missed.

Therefore, we're going to ask the new participant to _acknowledge_ our message and keep sending it until they do
(for a while, anyway):

```javascript
let pendingAck = {};

function ryhHandleParticipantJoined(e) {
  try {
    // Send local hand state to the newly joined participant
    const sessionId = e.participant.session_id;

    let attemptsLeft = 10;
    let interval = setInterval(trySendState, 1000);
    pendingAck.sessionId = interval;

    function trySendState() {
      sendLocalHandStateAppMessage(sessionId);
      attemptsLeft--;
      if (attemptsLeft === 0) {
        clearInterval(interval);
        console.warn(
          `failed to send local hand state to ${sessionId}; recipient never acknowledged`
        );
      }
    }
  } catch (err) {
    handleError("participant-joined", err);
  }
}
```

Above, we set up an _interval_ during which we'll keep sending our local hand state to the newly joined participant.
We will send every second for up to ten attempts. We will add the interval ID to the `pendingAck` object, keyed on their
session ID.

If after ten attempts we haven't received an acknowledgement from the participant, we will give up and clear the interval.

### participant-left

When a participant leaves the meeting, we will remove them from our list of participants in the UI:

```javascript
function removeParticipant(id) {
  const li = document.getElementById(id);
  if (li) {
    li.remove();
  }
}
```

### participant-updated:

Our participant UI displays the names of participants in the call (including the local user). To make our life a little
trickier (but more fun), participants are able to change their names mid-call. Such a name change would trigger a
`participant-updated` event which we can use to update our UI as follows:

```javascript
function ryhHandleParticipantUpdated(e) {
  try {
    const p = e.participant;
    if (p.local && p.user_name != userData.name) {
      userData.name = p.user_name;
      updateLocalParticipant();
    } else if (!p.local) {
      updateParticipantName(p.session_id, p.user_name);
    }
  } catch (err) {
    handleError("participant-updated", err);
  }
}
```

When the local participant updates their name, we will update our `userData` object to reflect the change.

If the update came from a remote participant, we'll assume that it is their _name_ they are updating because that's the
only relevant part for our `Raise Your Hand` UI. Therefore, we'll run the following function to reflect any possible
name change:

```javascript
function updateParticipantName(id, name) {
  console.log(`Updating participant name for ID: ${id}; name: ${name}`);
  const participantLi = document.getElementById(id);
  if (participantLi) {
    const participantNameEle = participantLi.getElementsByClassName("name");
    if (participantNameEle.length == 1) {
      participantNameEle[0].textContent = name;
    }
  }
}
```

Above, we try to find the list item associated with the given participant. If we find one, we'll grab the `name` span
from within it and set the inner text to the given name. This ensures that if a participant who has raised their hand
changes their name, the name will be updated in other participants' UI.

### app-message

When we receive an app message from another participant, we will expect it to either be a hand state update or an
acknowledgement of a hand state update. We will check which one it is based on the message `kind`.

```javascript
function ryhHandleAppMessage(e) {
  const data = e.data;
  console.log(
    `received msg of kind ${data.kind} from ${e.fromId}. Hand Raised: ${data.handRaised}`
  );
  if (data.kind === "ack") {
    if (pendingAck.sessionId) {
      clearInterval(pendingAck.sessionId);
      delete pendingAck.sessionId;
    }
    return;
  }
  if (data.kind === "update") {
    updateParticipantHandState(e.fromId, data.handRaised);
    if (data.wantAck) {
      sendAck(e.fromId);
    }
  }
}
```

In case of an acknowledgement (`ack` message kind), we'll try to find the interval associated with the sender's
session ID in our `pendingAck` object. If we find one, we'll clear the interval and stop sending them updates.

If the message is of kind `update`, we'll update the sender's hand state in our UI and (if requested) send the sender
our own message of acknowledgement.

```javascript
function updateParticipantHandState(id, handRaised) {
  if (!handRaised) {
    removeParticipant(id);
    return;
  }

  let participants = callFrame.participants();
  let p = participants[id];
  if (!p) {
    console.warn("could not find participant id " + id);
    return;
  }
  addParticipant(id, p.user_name);
  return;
}
```

Above, you can see that we retrieve the participant being updated from the call frame to get their name. We could
also get the participant sending us the message to include their name in the message data, but that would require
trusting their client to send their valid name. It is safer to get the participant's name from the `callFrame` itself.

When updating the participant's hand state, we will remove the participant from our UI if they are lowering their hand:

```javascript
function removeParticipant(id) {
  const div = document.getElementById(id);
  if (div) {
    div.remove();
  }
}
```

We'll add them if their hand is being raised:

```javascript
function addParticipant(id, name) {
  // This participant is already added (maybe we just haven't acknowledged their message yet)
  let li = document.getElementById(id);
  if (li) {
    return;
  }
  const otherParticipantsEle = document.getElementById("other-participants");
  li = document.createElement("li");
  otherParticipantsEle.appendChild(li);
  li.id = id;

  const nameSpan = document.createElement("span");
  nameSpan.className = "name";
  nameSpan.textContent = name;
  li.appendChild(nameSpan);

  const stateSpan = document.createElement("span");
  stateSpan.className = "state";
  stateSpan.textContent = handEmoji;
  li.appendChild(stateSpan);
}
```

We only want to add the participant if it hasn't already been added, so if a list item with their session ID already
exists, we early out.

## Finished!

And we're done! We now have a working `Raise Your Hand` feature for your Daily video calls.
